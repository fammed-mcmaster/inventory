import os
import sys

reload(sys).setdefaultencoding("UTF-8")

from setuptools import setup, find_packages


def read(*pathnames):
    return open(os.path.join(os.path.dirname(__file__), *pathnames)).read().\
        decode('utf-8')

setup(name='Products.inventory',
      version='0.1',
      description='Plone add-on providing inventory content types',
      url='https://gitlab.com/fammed-mcmaster/inventory',
      long_description='\n'.join([
          read('README.rst'),
          read('CHANGES.rst'),
          ]),
      classifiers=[
          'Framework :: Plone',
          'Framework :: Zope2',
          'Programming Language :: Python',
          ],
      keywords='plone inventory web',
      author='Servilio Afre Puentes',
      author_email='afrepues@mcmaster.ca',
      license='GPLv2+',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      namespace_packages=['Products'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'Products.CMFPlone >=4,<5',
      ],
      entry_points={
          'z3c.autoinclude.plugin': ['target = plone'],
      },
      )
