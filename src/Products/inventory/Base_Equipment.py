# File: Base_Equipment.py
#
# Copyright (c) 2006 by ['']
#
# GNU General Public Licence (GPL)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#
__author__ = ''' <>'''
__docformat__ = 'plaintext'


from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *

from Products.inventory.config import *


schema = Schema((
    StringField(
        'title',
        widget=StringWidget(
            label='ID',
            label_msgid='inventory_label_ID',
            description_msgid='inventory_help_ID',
            i18n_domain='Products.inventory',
        ),
        required=1,
        accessor="Title",
        searchable="1",
    ),

    StringField(
        'Serial NO',
        index="serialno:schema",
        widget=StringWidget(
            label='Serial NO',
            label_msgid='inventory_label_Serial NO',
            description_msgid='inventory_help_Serial NO',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'Model',
        index="Model:schema",
        widget=StringWidget(
            label='Model',
            label_msgid='inventory_label_Model',
            description_msgid='inventory_help_Model',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'DOP',
        index="DOP:schema",
        widget=StringWidget(
            label='DOP',
            label_msgid='inventory_label_DOP',
            description_msgid='inventory_help_DOP',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'Warranty Expiry',
        index="Warranty Expiry:schema",
        widget=StringWidget(
            label='Warranty Expiry',
            label_msgid='inventory_label_Warranty Expiry',
            description_msgid='inventory_help_Warranty Expiry',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

), )


class Base_Equipment(BaseContent):
    security = ClassSecurityInfo()

    allowed_content_types = []
    schema = schema
