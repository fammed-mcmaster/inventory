# File: Computer.py
#
# Copyright (c) 2006 by ['']
#
# GNU General Public Licence (GPL)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#
__author__ = ''' <>'''
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *

from Base_Equipment import Base_Equipment

from Products.inventory.config import *


schema=Schema((
    StringField(
        'OS',
        index="OS:schema",
        widget=StringWidget(
            label='OS',
            label_msgid='inventory_label_OS',
            description_msgid='inventory_help_OS',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'CPU',
        index="CPU:schema",
        widget=StringWidget(
            label='CPU',
            label_msgid='inventory_label_CPU',
            description_msgid='inventory_help_CPU',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'RAM',
        index="RAM:schema",
        widget=StringWidget(
            label='RAM',
            label_msgid='inventory_label_RAM',
            description_msgid='inventory_help_RAM',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'HD',
        index="HD:schema",
        widget=StringWidget(
            label='Hard Drive',
            label_msgid='inventory_label_HD',
            description_msgid='inventory_help_HD',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'NIC',
        index="NIC:schema",
        widget=StringWidget(
            label='NIC',
            label_msgid='inventory_label_NIC',
            description_msgid='inventory_help_NIC',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'Software',
        index="software:schema",
        widget=StringWidget(
            label='Software',
            label_msgid='inventory_label_Software',
            description_msgid='inventory_help_Software',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'PC Name',
        index="pcname:schema",
        widget=StringWidget(
            label='PC Name',
            label_msgid='inventory_label_PC Name',
            description_msgid='inventory_help_PC Name',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'IP',
        index="IP:schema",
        widget=StringWidget(
            label='IP Address',
            label_msgid='inventory_label_IP',
            description_msgid='inventory_help_IP',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'User',
        index="User:schema",
        widget=StringWidget(
            label='User',
            label_msgid='inventory_label_User',
            description_msgid='inventory_help_User',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'Other Information',
        index="Other Information:schema",
        widget=StringWidget(
            label='Other information',
            label_msgid='inventory_label_Other Information',
            description_msgid='inventory_help_Other Information',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

), )


class Computer(Base_Equipment, BaseContent):
    security = ClassSecurityInfo()

    # This name appears in the 'add' box
    archetype_name = 'Computer'

    meta_type = 'Computer'
    portal_type = 'Computer'
    allowed_content_types = [] + list(getattr(Base_Equipment, 'allowed_content_types', []))
    filter_content_types = 0
    global_allow = 0
    allow_discussion = 0
    #content_icon = 'Computer.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    _at_rename_after_creation = True
    typeDescription = "Computer"
    typeDescMsgId = 'description_edit_computer'

    schema = BaseSchema + \
        getattr(Base_Equipment, 'schema', Schema(())) + \
        schema


registerType(Computer, PROJECTNAME)
