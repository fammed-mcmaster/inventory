# File: Computer_Folder.py
#
# Copyright (c) 2006 by ['']
#
# GNU General Public Licence (GPL)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#
__author__ = ''' <>'''
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.ATContentTypes.content.folder import ATFolder

from Products.inventory.config import *


schema = ATFolder.schema


class Computer_Folder(OrderedBaseFolder, ATFolder):
    security = ClassSecurityInfo()

    # This name appears in the 'add' box
    archetype_name = 'Computer Folder'

    meta_type = 'Computer Folder'
    portal_type = 'Computer Folder'
    allowed_content_types = ['Importer', 'Computer']
    filter_content_types = 1
    global_allow = 0
    allow_discussion = 0
    #content_icon = 'Computer_Folder.gif'
    immediate_view = 'folder_listing'
    default_view = 'folder_listing'
    suppl_views = (
        'folder_summary_view',
        'folder_tabular_view',
        'atct_album_view',
        )
    _at_rename_after_creation = True
    typeDescription = "Computer Folder"
    typeDescMsgId = 'description_edit_computer folder'

    schema = OrderedBaseFolderSchema + schema


registerType(Computer_Folder, PROJECTNAME)
