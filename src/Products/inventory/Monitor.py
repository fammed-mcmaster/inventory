# File: Monitor.py
#
# Copyright (c) 2006 by ['']
#
# GNU General Public Licence (GPL)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#
__author__ = ''' <>'''
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *

from Base_Equipment import Base_Equipment

from Products.inventory.config import *


schema = Schema((
    StringField(
        'Size',
        index="Size:schema",
        widget=StringWidget(
            label='Size',
            label_msgid='inventory_label_Size',
            description_msgid='inventory_help_Size',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

    StringField(
        'Other Information',
        index="Other Information:schema",
        widget=StringWidget(
            label='Other information',
            label_msgid='inventory_label_Other Information',
            description_msgid='inventory_help_Other Information',
            i18n_domain='Products.inventory',
        ),
        searchable="1",
    ),

), )


class Monitor(Base_Equipment, BaseContent):
    security = ClassSecurityInfo()

    # This name appears in the 'add' box
    archetype_name = 'Monitor'

    meta_type = 'Monitor'
    portal_type = 'Monitor'
    allowed_content_types = [] + list(getattr(Base_Equipment, 'allowed_content_types', []))
    filter_content_types = 0
    global_allow = 0
    allow_discussion = 0
    #content_icon = 'Monitor.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    _at_rename_after_creation = True
    typeDescription = "Monitor"
    typeDescMsgId = 'description_edit_monitor'

    schema = BaseSchema + \
        getattr(Base_Equipment, 'schema', Schema(())) + \
        schema


registerType(Monitor, PROJECTNAME)
