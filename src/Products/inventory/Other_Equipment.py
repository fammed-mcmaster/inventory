# File: Other_Equipment.py
#
# Copyright (c) 2006 by ['']
#
# GNU General Public Licence (GPL)
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#
__author__ = ''' <>'''
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.ATContentTypes.content.folder import ATFolder

from Products.inventory.config import *


schema = ATFolder.schema


class Other_Equipment(OrderedBaseFolder, ATFolder):
    security = ClassSecurityInfo()

    # This name appears in the 'add' box
    archetype_name = 'Other Equipment'

    meta_type = 'Other Equipment'
    portal_type = 'Other Equipment'
    allowed_content_types = ['Importer', 'Equipment']
    filter_content_types = 1
    global_allow = 0
    allow_discussion = 0
    #content_icon = 'Other_Equipment.gif'
    immediate_view = 'folder_listing'
    default_view = 'folder_listing'
    suppl_views = (
        'folder_summary_view',
        'folder_tabular_view',
        'atct_album_view',
        )
    _at_rename_after_creation = True
    typeDescription = "Other Equipment"
    typeDescMsgId = 'description_edit_other equipment'

    schema = OrderedBaseFolderSchema + schema


registerType(Other_Equipment, PROJECTNAME)
