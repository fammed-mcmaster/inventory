#
# Initialise the product's module.
#

from zLOG import LOG, INFO

from Products.CMFCore import utils
from Products.Archetypes.public import *
from Products.Archetypes import listTypes

from Products.inventory.config import *


LOG('Products.inventory', INFO, 'Installing Product')


def initialize(context):
    # imports packages and types for registration

    import Inventory_Folder
    import Computer_Folder
    import Monitor_Folder
    import Printer_Folder
    import Computer
    import Monitor
    import Printer
    import Base_Equipment
    import Other_Equipment
    import Equipment

    # initialize portal content
    content_types, constructors, ftis = process_types(
        listTypes(PROJECTNAME),
        PROJECTNAME)

    utils.ContentInit(
        PROJECTNAME + ' Content',
        content_types=content_types,
        permission=DEFAULT_ADD_CONTENT_PERMISSION,
        extra_constructors=constructors,
        fti=ftis,
        ).initialize(context)
