#
# Product configuration. This contents of this module will be imported into
# __init__.py and every content type module.
#
from Products.CMFCore.permissions import setDefaultRoles


PROJECTNAME = "Products.inventory"

DEFAULT_ADD_CONTENT_PERMISSION = "Add portal content"
setDefaultRoles(DEFAULT_ADD_CONTENT_PERMISSION, ('Manager', 'Owner', 'Member'))

product_globals = globals()
